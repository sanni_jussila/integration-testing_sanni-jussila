// TDD - Test Driven Development - Unit testing

const expect = require('chai').expect;
const converter = require('../src/converter');

describe("Color code converter", () => {
    describe("RBG <-> Hex conversion", () => {
        it("converts RGB to hex", () => {
            const redHex = converter.rgbToHex(255, 0, 0); // ff0000
            const greenHex = converter.rgbToHex(0, 255, 0); // 00ff00
            const blueHex = converter.rgbToHex(0, 0, 255); // 0000ff

            expect(redHex).to.equal("ff0000")
            expect(greenHex).to.equal("00ff00")
            expect(blueHex).to.equal("0000ff")
        })

        it("converts hex to RGB", () => {
            const HexOne = converter.hexToRGB("ff0000")

            expect(HexOne).to.equal("255, 0, 0")
        })

    })
})


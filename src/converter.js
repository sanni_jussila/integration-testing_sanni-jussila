const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex );
}

module.exports = {
    rgbToHex: (red, green, blue) => {
        const redHex = red.toString(16); // "02"
        const greenHex = green.toString(16);
        const blueHex = blue.toString(16);

        return pad(redHex) + pad(greenHex) + pad(blueHex); // ff0000
    } ,
    hexToRGB: (hex) => {
        const r = parseInt(hex[0] + hex[1], 16);
        const g = parseInt(hex[2] + hex[3], 16);
        const b = parseInt(hex[4] + hex[5], 16);

        return r + ", " + g + ", " + b
    }
}
